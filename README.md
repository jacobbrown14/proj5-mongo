# README #

Author: Jacob Brown, jbrown14@uoregon.edu
Project 5, 11/12/2018, for CIS 322, Fall 2018

## Description: 

A Flask and MongoDB implementation of an ACP Brevet Control Times Calculator. Set the Brevet distance, starting time and date, and then set the control location. The program will calculate the opening and closing times using these values, store them in a database, and display them on a separate page. Further explanation of the algorithm can be found below (at "Algorithm")

## Instructions: 

(Docker) Use start.sh to build and run Docker and connect to MongoDB. Go to "localhost:5000" in a browser to see the submission form page. Enter the values, then click "Submit." Click on "Display" to see all of your submissions calculated with the correct opening and closing times.

Use CTRL+Z to exit to bash. There you can use stop.sh to stop ALL running Docker containers.

## Testing:

Although no testing programs exist for this project, you can try the following scenarios yourself:

(Empty Database) Without submitting anything yet, click on the "Display All" button. You will be routed to the an empty display page with the message "WARNING: Empty database. Go back and try again."

(No Control Entered) Go to the submission page and click the "Submit" button without entering anything in the "Control Distance (km)" field. You will be routed to an error page that says "ERROR: No control distance was entered"

(Unreasonable Control Distance) Enter a control distance greater than 1300 (the maximum distance of a brevet) to see the message "ERROR: Control distance is greater than brevet maximum distance"

## Intro:

(Paraphrased from Wikipedia): Randonneuring a long-distance sport in cycling. A randonneuring event is referred to as a "brevet." These brevets have checkpoints - called "controls" - throughout the length of the brevet.

## Algorithm:

Follow along using the table found here: https://rusa.org/pages/acp-brevet-control-times-calculator

- Starting from zero, for each row the control distance doesn't fall into, subtract the low range from the high range, and divide this value by its speed. Each of these values are added together.

- For the row that the control does fall into, subtract the low range from the control, then divide by its speed. Then add this value to the result from the previous step.

- The fractional portion of your result will need to be multiplied by 60, then rounded to the nearest integer. This result will represent the minutes, while the whole numbers will represent the hours. They'll follow this template: 00H00

- Add this number to your starting time to see the final open/close time.

Example (using an 890km brevet on a 1000km brevet):

For the opening times, we use values in the Maximum Speed column.

- (200 / 34) + (200 / 32) + (200 / 30) + ((890 - 600) / 28) = 29.1561624...

- 0.1561624 * 60 = 9.36972

- 29H09

For the closing times, we use the values in the Minimum Speed column.

- (200 / 15) + (200 / 15) + (200 / 15) + ((890 - 600) / 11.428) = 65.37626...

- 0.37626 * 60 = 22.576128

- 65H23

## Algorithm Misc:

- As per the rules, the closing time at the start of a brevet will always be set one hour after the starting time. A note will appear in the calculator if your control distance is set to zero.

- Control distances between 0% and 20% of the brevet distance will instead use the brevet distance in its calculation. Control distances beyond 20% of the brevet distance will calculate normally albeit incorrectly and will display an error message in the "Notes" column.
